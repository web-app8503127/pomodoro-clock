let countdown=document.getElementById("countdown");
let label=document.createElement("label");
label.innerHTML="00 : 00";
countdown.appendChild(label);

let timeid,mm,ss,seconds,check=1,pauseCheck=0;

function startTimer(){
  if(st==0 || bt==0)
  {
    alert("Please set both time atleast at 1 min");
    resetTimer();
    return;
  }
  if(pauseCheck==1)
  {
    seconds=localStorage.getItem("seconds");
    pauseCheck=0;
  }  
  else
    seconds=st*60;
    
  if(seconds==0)
  {
    resetTimer();
    return;
  }  
  timeid=setInterval(function(){
    seconds--;
    mm=parseInt(seconds/60);
    ss=parseInt(seconds%60);
    if(mm==0 && ss==0 && check==1)
    {
      document.getElementsByClassName("session")[0].innerHTML="Break!";
      check=2;
      seconds=bt*60;
      label.style.color= "#A4553A";
      countdown.style.borderColor="EB6841";
    }    
    else if(mm==0 && ss==0 && check==2)
    {
      document.getElementsByClassName("session")[0].innerHTML="Session 1";
      check=1;
      seconds=st*60;
      label.style.color="#00A0B0";
      countdown.style.borderColor="#00A0B0";
    }
    if(mm<10)
      mm="0"+mm;
    if(ss<10)
      ss="0"+ss;  
    label.innerHTML=mm+" : "+ss;
  },1000);
}
function pauseTimer(){
  pauseCheck=1;
  localStorage.setItem("seconds",seconds);  
  clearInterval(timeid);
}

function resetTimer(){
  clearInterval(timeid);
  label.innerHTML="00 : 00";
  sminus.disabled=false;
  splus.disabled=false;
  bminus.disabled=false;
  bplus.disabled=false;
  start.innerHTML="Start";
  check=1;
  pauseCheck=0;
  label.style.color="#00A0B0";
  countdown.style.borderColor="#00A0B0"
}

let schedule=document.getElementById("timing");

let session=document.createElement("span");
session.innerHTML="session time";

let breaktime=document.createElement("span");
breaktime.innerHTML="break time";

schedule.append(session,breaktime,document.createElement('br'));

let stime=document.createElement("span");
stime.setAttribute("id","stime");
let st=20;
stime.innerHTML=st +" min";

let btime=document.createElement("span");
btime.setAttribute("id","btime");
let bt=5;
btime.innerHTML=bt+" min";

schedule.append(stime,btime,document.createElement("br"));

let sminus=document.createElement("button");
sminus.setAttribute("class","decrease");
sminus.innerHTML="-";
sminus.addEventListener("click",stimedec);

let splus=document.createElement("button");
splus.setAttribute("class","increase");
splus.innerHTML="+";
splus.addEventListener("click",stimeinc);

let bminus=document.createElement("button");
bminus.setAttribute("class","decrease");
bminus.innerHTML="-";
bminus.addEventListener("click",btimedec);

let bplus=document.createElement("button");
bplus.setAttribute("class","increase");
bplus.innerHTML="+";
bplus.addEventListener("click",btimeinc);
schedule.append(sminus,splus,bminus,bplus)

function stimedec(){
  st--;
  if(st<1)
  {
    alert("Session Time cannot be below 1");
    st=1;
  }
  stime.innerHTML=st +" min";
}
function stimeinc(){
  st++;
  stime.innerHTML=st +" min";
}
function btimedec(){
  bt--;
  if(bt<1)
  {
    alert("Break Time cannot go below 1");
    bt=1;
  }
  btime.innerHTML=bt+" min";
}
function btimeinc(){
  bt++;
  btime.innerHTML=bt+" min";
}
let control=document.getElementById("controller");

let start=document.createElement("button");
start.setAttribute("id","start");
start.innerHTML="Start";
start.addEventListener("click",checkstate1);

let reset=document.createElement("button");
reset.setAttribute("id","reset");
reset.innerHTML="Reset";
reset.addEventListener("click",resetTimer);
control.append(start,reset);

function checkstate1(){
  if(start.innerHTML=="Start")
  {
    if(pauseCheck!=1)
    {
      if(check==1)
      {
        if(st<10)
          label.innerHTML="0"+st+" : 00";
        else
          label.innerHTML=st+" : 00";
      }
      else
      {
        if(bt<10)
          label.innerHTML="0"+bt+" : 00";
        else
          label.innerHTML=bt+" : 00";
      }
    }      
    sminus.disabled=true;
    splus.disabled=true;
    bminus.disabled=true;
    bplus.disabled=true;
    start.innerHTML="Pause";
    startTimer();
  }
  else if(start.innerHTML=="Pause")
  {
    start.innerHTML="Start";
    pauseTimer();
  }
}